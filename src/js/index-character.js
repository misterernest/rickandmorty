const API = 'https://rickandmortyapi.com/api/character/';

const getData = async () => {
    try {
        const response = await fetch(API);
        const data = await response.json();
        return data
    } catch (e) {
        console.error(`Error en funcion getData error: ${e}`);
    }
}

const charactersMainSection = async () => {
    const characters = await getData();
    let charactersContainer = document.getElementById('characters-container');
    charactersContainer.innerHTML =characters.results.map( character => `
        <article class="card">
            <figure class="character">
                <img src="${character.image}" alt="${character.name}">
            </figure>
            <div class="character__info">
                <h3>${character.name}</h3>
                <div>
                    <span class="status">${character.status}</span>- <span class="species">${character.species}</span>
                </div>
                <span class="label-location">Last known location:</span>
                <span class="location-name">${character.origin.name}</span>
                <span class="label-episode">First seen in:</span>
                <span class="first-episode"><a href="${character.episode[0]}"></a></span>
            </div>
        </article>
    `).join('')
}

charactersMainSection();
